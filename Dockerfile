FROM python:3.7

MAINTAINER Jean POMMIER "jean.pommier@pi-geosolutions.fr"
# follow the setup described at https://addok.readthedocs.io/en/latest/tutorial/

RUN apt-get update && \
    apt-get install -y \
        git \
        redis-tools \
        uwsgi-plugin-python3 \
    && \
    rm -rf /var/lib/apt/lists/*

RUN python3 -m pip install cython addok==1.0.2 addok-csv==1.0.1 uwsgi

# create addok user. Will be used to run the server
RUN useradd -N addok -m -d /srv/addok/ \
    && chsh -s /bin/bash addok

#Create config folder
RUN mkdir /etc/addok/ \
    && chown addok /etc/addok

VOLUME ["/data"]
# expose addok port
EXPOSE 7878

COPY docker/addok/resources/root/ /
COPY docker/addok/resources/addok/ /srv/addok/
COPY --chown=addok:users data/KSK_adresy.csv /data/KSK_adresy.csv
COPY --chown=addok:users data_preparation/ /data_preparation/
RUN python3 -m pip install -r /data_preparation/requirements.txt
#RUN ln -s /srv/addok/uwsgi.ini /etc/uwsgi/apps-enabled/addok.ini

RUN chmod +x /docker-entrypoint.sh /docker-entrypoint.d/*

USER addok

#WORKDIR "/srv/addok"
WORKDIR "/data"
ENV CSV_INPUT_FILE=/data/KSK_adresy.csv
ENV SJSON_OUTPUT_FILE=/data/KSK_adresy.sjson
ENV ADDOK_CONFIG_MODULE=/srv/addok/addok.conf

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["uwsgi",   "--ini", "/srv/addok/uwsgi.ini", \
                "--http", "0.0.0.0:7878" ]
