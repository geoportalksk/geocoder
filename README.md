# Geocoding service for KSK, based on SK register of addresses

This code is intended to work with the CSV data produced by KSK GIS team, based of national register of addresses.
An example of the generated CSV is available in the data folder.

The geocoding and reverse-geocoding service is based on [addok](https://addok.readthedocs.io/en), a geocoding solution implemented and used by Etalab, in France.

The script located in data_preparation is automatically run by the docker container on startup, transforming the data into a suitable format, for ingestion into addok.
It is then ingested and indexed, resulting in a ready-to-work geocoder container.

To override the input data, you can mount another file, and use it by declaring the environment variable `CSV_INPUT_FILE`. And restart the container.

## Build the service
This service mostly targets a docker-like environment. The data preparation script can be run separately, but the rest is intended to be used in docker (or kubernetes) context.

To build the full service, use the `docker-compose build` command, or use the Makefile: `make docker-build`

## Run the service
Locally, you can run `docker-compose up` to get it running

## Usage
    # start the service
    docker-compose up -d
    # run a query
    http -f GET http://localhost:7878/search/?q=Gelnica
    # geocode a CSV file (see https://github.com/addok/addok-csv)
    http -f POST http://localhost:7878/search/csv/ columns='Adresa žiadateľa' data@dotacie_utf8.csv

## Work on the data preparation script
    # Create a virtualenv
    python3 -m virtualenv .venv
    # Activate it
    source .venv/bin/activate
    # Install dependecies
    pip install -r data_preparation/requirements.txt
    # Run the script
    # note: you can configure how the script will behave by changing some oe the environment variables, see below, or in the code
    python3 addok_preformat_ksk_addresses.py 

## Configuration
The following environment variables are available:
* `REDIS_HOST`: allow to change the default host. Default: 'addok-redis'
* `REDIS_PORT`: allow to change the default port. Default: 6379
* `CSV_INPUT_FILE`: path to the input CSV file. Default: 'data/KSK_adresy.csv'
* `CSV_ENCODING`: encoding of the CSV file. Default: 'Windows-1250'
* `CSV_DELIMITER`: CSV delimiter. Default: ';'
* `TARGET_REGION`: the script limits the scan to a single slovak region. Default: 'Košický'
* `SJSON_OUTPUT_FILE`: path to the generated file (sjson output), that will serve as input for addok. Default 'data/KSK_adresy.sjson'
* `PROM_PUSHGATEWAY_URI`: some Prometheus metrics are generated. If the URL for the pushgateway is provided, they will be pushed there. This is optional. If no URL is provided, nothing will happen.
* `LOG_LEVEL`: Logs level. Defaults to 'INFO'. To get more information, you can set it to 'DEBUG'