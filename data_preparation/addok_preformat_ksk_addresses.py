# Prepare the KSK addresses for addok (https://github.com/addok/addok) ingestion
# Takes a CSV file (list of street addresses) in Windows 1250 encoding
# Generates a sjson file suitable for ingestion by addok, in UTF8 encoding

import csv
import json
import logging
import sys
from datetime import datetime
from os import environ

from prometheus_client import CollectorRegistry, Histogram, Counter, Gauge, push_to_gateway

# Parse environment variables
csv_input_file=environ.get('CSV_INPUT_FILE', 'data/KSK_adresy.csv')
csv_source_encoding=environ.get('CSV_ENCODING', 'Windows-1250')
csv_delimiter=environ.get('CSV_DELIMITER', ';')
target_region= environ.get('TARGET_REGION', 'Košický')
sjson_output_file=environ.get('SJSON_OUTPUT_FILE', 'data/KSK_adresy.sjson')
prom_pushgateway_uri = environ.get('PROM_PUSHGATEWAY_URI') # if not defined, it won't send metrics
loglevel = environ.get('LOG_LEVEL', logging.INFO)

# Logging configuration
log = logging.getLogger(__name__)
log.setLevel(loglevel)
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))
log.addHandler(handler)

# Prometheus metrics
registry = CollectorRegistry()
prom_valid_records = Gauge('valid_records', 'Number of valid records, ready for ingestion into Addok', registry=registry)
prom_invalid_records = Gauge('invalid_records', 'Number of record that were discarded (invalid)', registry=registry)
prom_last_run = Gauge('job_last_success_unixtime', 'Last time the job was run', registry=registry)
prom_process_duration_seconds = Histogram('process_duration_seconds', 'Duration in seconds of the job process', registry=registry)

counters = {
    'valid_records': 0,
    'invalid_records': 0
}

def csv_to_sjson(input_filename, output_filename):
    '''
    Parse a CSV file (expected in Windows 1250 encoding, delimiter ;
    Produce a sjson file intended for ingestion into addok
    Processes in-memory: if the file is really too big, it might need some improvement
    :param input_filename:
    :param output_filename:
    :return:
    '''
    with open(input_filename, newline='', encoding=csv_source_encoding) as fi:
        csv_reader = csv.reader(fi, delimiter=csv_delimiter)
        next(csv_reader)
        records=dict()
        for row in csv_reader:
            #print(row)
            if (row[1]==target_region):
                # Add from the district level (we consider everything is within one region)
                _add(row, records)

    with open(output_filename, 'w') as fo:
        for d_key, d_items in records.items():
            for m_key, m_items in d_items.items():
                for s_key, s_items in m_items.items():
                    json_street = _street_to_json(d_key, m_key, s_key, s_items)
                    if json_street:
                        fo.writelines(json_street + '\n')


def _add(row, records):
    '''
    Recursively creates the nested structure: okres (district), obec (municipality), Ulica (street), Supisne Cislo (numbers)
    :param row: CSV row, one record
    :param records: nested dictionnary of the addresses (closer to targeted sjson structure)
    :return:
    '''
    global counters
    try:
        if not row[2]:
            # district name is empty string
            log.debug('invalid line (missing district name): {}'.format(row))
            counters['invalid_records'] += 1
            return
        district = _get_dict_or_initialize(records, row[2])

        if not row[3]:
            # municipality name is empty string
            log.debug('invalid line (missing municipality name): {}'.format(row))
            counters['invalid_records'] += 1
            return
        municipality = _get_dict_or_initialize(district, row[3])

        if not row[5]:
            # street name is empty string
            log.debug('invalid line (missing street name): {}'.format(row))
            counters['invalid_records'] += 1
            return
        street = _get_dict_or_initialize(municipality, row[5])
        street['postcode'] = row[8]

        housenumbers = _get_dict_or_initialize(street, 'housenumbers')

        if row[6] and row[9] and row[10] :
            street['housenumbers'][row[6]] = {
                'lon': float(row[9].replace(',', '.')),  # hack around the decimal separator potentially being a comma
                'lat': float(row[10].replace(',', '.'))
            }
            counters['valid_records'] += 1
        else:
            log.debug('invalid line {}'.format(row))
            counters['invalid_records'] += 1
    except:
        log.error("Unexpected error: {}".format(sys.exc_info()[0]))


def _street_to_json(district_name, municipality_name, street_name, street_dict):
    '''
    Write the street record as a one-liner json string (will be one line in the whole sjson document)
    :param district_name:
    :param municipality_name:
    :param street_name:
    :param street_dict:
    :return:
    '''
    housenumbers = street_dict.get('housenumbers')
    if len(dict(housenumbers)) == 0:
        return ''

    if not street_name or not municipality_name or not district_name:
        return ''

    street = {
        'type': 'street',
        'street': street_name,
        'name': street_name,
        'city': municipality_name,
        'postcode': street_dict.get('postcode'),
        'district': district_name,
        'region': target_region,
        'importance': 0.5,
        'lat': list(housenumbers.values())[0]['lat'],
        'lon': list(housenumbers.values())[0]['lon'],
        'housenumbers': housenumbers
    }
    return json.dumps(street)


def _get_dict_or_initialize(parent_dict, key):
    my_dict = parent_dict.get(key)
    if my_dict is None:
        # initialize the object for this district
        my_dict = dict()
        parent_dict[key] = my_dict
    return my_dict



@prom_process_duration_seconds.time()
def main():
    global counters
    csv_to_sjson(csv_input_file, sjson_output_file)
    log.info('processing finished with {} valid lines and {} invalid (unusable) lines'.format(counters['valid_records'], counters['invalid_records']))

    # Send metrics to prometheus pushgateway
    if prom_pushgateway_uri:
        try:
            prom_valid_records.set(counters['valid_records'])
            prom_invalid_records.inc(counters['invalid_records'])
            prom_last_run.set_to_current_time()
            push_to_gateway(prom_pushgateway_uri, job='geocoder_csv_to_sjson', registry=registry)
        except Exception:
            log.error("Unable to reach prometheus pushgateway {}".format(prom_pushgateway_uri))


if __name__ == '__main__':
    main()

